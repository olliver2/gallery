<?
	include "server/dbcon.php";
	include "server/error.php";
	if(isset($_GET['id'])){
		$id=(int)base64_decode($_GET['id']);
	}
	$sql="SELECT id,title,description,img FROM works WHERE id = :id";
	$sql2="SELECT id FROM works";

	foreach ($dbcon->query($sql2) as $works) {
		if($id == $works['id']){
			$noid=$id;
		}
	}
	if(empty($id) or $id !== $noid){
		header('Location: http://djuminagallery.esy.es/gallery.php');
		exit;
	}
	$sth=$dbcon->prepare($sql);
	$sth->execute( array('id'=>$id));
	$result=$sth->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Инормационная страница</title>
	<meta charset="utf-8">
	<meta name="Description" content="Яна Дюмина,начинающий и очень талантливый художник">
	<meta name="Keywords" content="Яна Дюмина, Галерея Яны Дюминой , работы на холсте, работы маслом, начинающий художник">
		<script type="text/javascript" src="js/jquery.js"></script>
		<link rel="stylesheet" type="text/css" href="css/info.css">
</head>
<body>
		<div class="content">
		<? foreach($result as $work ){ ?>
			<div class="image">
				<img src="image/<? echo $work['img'];?>">
			</div>
			<div class="desc">
				<h1><? echo $work['title'];?></h1>
				<p><? echo nl2br($work['description']);?></p>
			</div>
		<? } ?>
		</div>
</body>
</html>