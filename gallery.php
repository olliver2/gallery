<?
	include "server/dbcon.php";
	include "server/error.php";
	$sql= "SELECT id,title,description,img FROM works";
	$result=$dbcon->query($sql);
	$count=$result->rowCount();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Галерея</title>
	<meta charset="utf-8">
	<meta name="Description" content="Яна Дюмина,начинающий и очень талантливый художник">
	<meta name="Keywords" content="Яна Дюмина, Галерея Яны Дюминой , работы на холсте, работы маслом, начинающий художник">
	<meta name="viewport" content="width=device-width , initial-scale=1.0">
        <script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/slide.js"></script>
        <link rel="stylesheet" type="text/css" href="css/gallery.css">
	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon">
</head>
<body>
	<div class="content">
		<div class="images">
		<?if($count!==0){ 
		foreach($dbcon->query($sql) as $work ){ ?>
			<a href="info.php?id=<? echo base64_encode($work['id']);?>" onclick="event.preventDefault()"><img class="image" src="image/<? echo $work['img'];?>"></a>
		<? } 
		}else{
			echo "<p class='count'>В галереи картин нету!</p>";
			}?>
		</div>
		<div class="modal">
				<img class="modalimg" src="">
			<div class="slide"> 
				<span class="prev"><img src="image/prev.png"></span>
				<span class="center" title="HO-HO-HO"><a href=""><img src="image/dart.ico"></a></span>
				<span class="next"><img src="image/next.png"></span>
			</div>
		</div>
	</div>
	<div class="shim"></div>
</body>
</html>