<?
 	include "server/dbcon.php";
 	include "server/delete.php"; 
 	include "server/error.php";
	$sql2="SELECT id,title,description,img FROM works ORDER BY id DESC";
?>
<!DOCTYPE html>
<html>
<head>
	<title>Янка-бананка</title>
	<meta charset="utf-8">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/edit.js"></script>
	<script type="text/javascript" src="js/add.js"></script>
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/adminstyle.css">
<link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon">
</head>
<body>
	<div class="contain">
		<div class="forms">
			<form id="one" action="<?echo $_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data">
				<input type="text" name="title" placeholder="Название картины" required><br>
				<textarea name="desc" placeholder="Описание картины" required ></textarea></br>
				<label for="imgfile"><img src="image/Master Joda.ico"></label></br>
				<span class="filename">Да прибудет с тобой СИЛА!</span></br>
				<input type="file" name="img" id="imgfile" value="" required /></br>
				<input type="submit" value="Добавить"></br>
			</form>
		</div>
		
		<? foreach ($dbcon->query($sql2) as $works) { ?>
			<div class="demoworks">
				<div class="content">
					<div class="pictures"><img src="image/<? echo $works['img'];?>"></div>
					<div class="title"><? echo $works['title'];?></div>
					<div class="delete">
						<a href="?del=<? echo $works['id'];?>">Удалить</a><br>
						<a href="<? echo $works['id'];?>" class="edit">Изменить</a><br>
					</div>
				</div>
				<div style="clear: both;"></div>
				<div class="description">
					<? echo nl2br($works['description']);?>
				</div>
			</div>
		<? } ?>
		
	</div>
	<div class="modal"></div>
	<div class="modalform">
		<form class="edits" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="id" value="">
			<input type="text" name="title" placeholder="Название картины" required><br>
			<textarea name="desc" placeholder="Описание картины" required ></textarea></br>
			<input  type="file" name="img" value="" required /></br>
			<input type="submit" value="Изменить"></br>
		</form>
	</div>
	<div class="shim"></div>
</body>
</html>