<!DOCTYPE html>
<html>
<head>
	<title>Главная</title>
<link rel="stylesheet" type="text/css" href="css/index.css">
        <meta charset="utf-8">
	<meta name="Description" content="Яна Дюмина,начинающий и очень талантливый художник">
	<meta name="Keywords" content="Яна Дюмина, Галерея Яны Дюминой , работы на холсте, работы маслом, начинающий художник">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon">
</head>
<body>
	<div class="contain">
		<div class="logo">
			<a href="index.php"><p class="para">Яна Дюмина</p><a>
		</div>
		<div class="menu">
			<a href="gallery.php">Галерея</a>
			<a href="contact.php">Контакты</a>
		</div>
	</div>
</body>
</html>