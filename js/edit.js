$(function(){
	function n2(str){
		return str.replace(/\n/g,"<br/>");
	}
	$(document).on('click','.edit',function(e){
			e.preventDefault();
			$('.shim').show();
			$(".modalform").fadeIn();
			$(".edits").show();
			var href=$(this).attr('href');
			$("input[type=hidden]").val(href);
		});
	$(document).on('submit','.edits',function(){
		$.ajax({
			url:"server/edits.php",
			type:"POST",
			data:new FormData(this),
			contentType: false,
			processData:false,
			success:function(html){
				$("textarea,input[type=file],input[type=text]").val("");
				$('.modalform').hide();
				$('.modal').show();
				$('.modal').html(html);
			},
		}).done(function(){
			$('.demoworks').remove();
			$.ajax({
				url:"server/select.php",
				dataType:"json",
				success:function(data){
					for(var i=0;i<data.length;i++){
						$('.contain').each(function(){
							$(this).append(
								"<div class=demoworks>"+
									"<div class=content>"+
										"<div class=pictures><img src='image/"+data[i].img+"'></div>"+
										"<div class=title>"+data[i].title+"</div>"+
										"<div class=delete>"+
											"<a href=?del="+data[i].id+">Удалить</a><br>"+
											"<a href="+data[i].id+" class='edit'>Изменить</a><br>"+
										"</div>"+
									"</div>"+
									"<div class=description>"+n2(data[i].description)+"</div>"+
								"</div>"
							);
						});
					}
				}
			});
		});
		return false;
	});
});