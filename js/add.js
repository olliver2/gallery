$(function() {
	function n2(str){
		return str.replace(/\n/g,"<br/>");
	}
	
	$("#one").bind('submit',function(e){
		$.ajax({
			url:"server/add.php",
			type:"POST",
			data:new FormData(this),
			contentType: false,
			processData:false,
			success:function(data){
				$("textarea,input[type=file],input[type=text]").val("");
				$('.shim').show();
				$('.modal').fadeIn();
				$('.modal').html(data);
			},
			error:function(){
				$('.message').html('Картина добавлена');
			}
		}).done(function(){
			$('.demoworks').remove();
			$.ajax({
				url:"server/select.php",
				dataType:"json",
				success:function(data){
					for(var i=0;i<data.length;i++){
						$('.contain').each(function(){
							$(this).append(
								"<div class=demoworks>"+
									"<div class=content>"+
										"<div class=pictures><img src='image/"+data[i].img+"'></div>"+
										"<div class=title>"+data[i].title+"</div>"+
										"<div class=delete>"+
											"<a href=?del="+data[i].id+">Удалить</a><br>"+
											"<a href="+data[i].id+" class='edit'>Изменить</a><br>"+
										"</div>"+
									"</div>"+
									"<div class=description>"+n2(data[i].description)+"</div>"+
								"</div>"
							);
						});
					}
				}
			});
		});
		return false;
	});
	$("label").bind('click',function(){
		$("input[type=file]").bind('change',function(){
		var fileval=$("input[type=file]").val();
		fileval=fileval.substring(12);
		if(fileval==""){
			$('.filename').html('Выбирите файл!');
			}else{
				$('.filename').html(fileval);
			}
		});
	});
	$('.shim').bind('click',function(){
		$('.modal').hide();
		$('.modalform').hide();
		$(this).fadeOut();
		$('.edits').hide();
	});
});