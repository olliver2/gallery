$(function(){
			$('.images a').bind('click',function(){
				var src=$(this).children().attr('src');
				var href=$(this).attr('href');
				$('.center a').attr('href',href);
				$('.modalimg').attr('src',src);
				$('.modal').fadeIn();

				$('.shim').show();

				$('.shim').bind('click',function(){
					$('.modal').hide();
					$(this).fadeOut();
				});


				$('.next').bind('click',function(){
					var currImg =$('.modalimg').attr('src');
					var currHref=$('.center a').attr('href');

					$('.images a').each(function(){
						if($(this).children().attr('src') == currImg && $(this).attr('href') == currHref){
							var nextHref =$(this).next().attr('href');
							var next = $(this).next().children().attr('src');
							$('.center a').attr('href',nextHref);
							$('.modalimg').fadeOut('fast',function(){
								$('.modalimg').attr('src',next);
								$('.modalimg').fadeIn('fast');
							});
							if($(this).next().length === 0){
								var first=$('.images a').first().children().attr('src');
								var firstHref=$('.images a').first().attr('href');
								$('.center a').attr('href',firstHref);
								$('.modalimg').fadeOut('fast',function(){
									$(this).attr('src',first);
									$(this).fadeIn('fast');
								});
							}
						}
					});
					currImg = next;
					currHref=nextHref;
				});
				$('.prev').bind('click',function(){
					var currImg =$('.modalimg').attr('src');
					var currHref=$('.center a').attr('href');
					$('.images a').each(function(){
						if($(this).children().attr('src') == currImg  && $(this).attr('href') == currHref){
							var prevHref=$(this).prev().attr('href');
							var prev = $(this).prev().children().attr('src');
							$('.center a').attr('href',prevHref);
							$('.modalimg').fadeOut('fast',function(){
								$(this).attr('src',prev);
								$(this).fadeIn('fast');
							});
								if($(this).prev().length === 0){
								var last=$('.images a').last().children().attr('src');
								var lastHref=$('.images a').last().attr('href');
								$('.center a').attr('href',lastHref);
								$('.modalimg').fadeOut('fast',function(){
									$(this).attr('src',last);
									$(this).fadeIn('fast');
								});
							}
						}
					});
					currImg = prev;
				});
			})
		});